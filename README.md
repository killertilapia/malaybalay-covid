# malaybalay-covid

Simple Dashboard displaying data about COVID-19 in the City of Malaybalay.

## Setup
1. Clone this repository
2. Install the required packages
3. Run the Django web application

## Requirements
* Database (This was developed using MariaDB)
* Python packages specified on the `requirements.txt`

## License
Code released under the [MIT License](https://gitlab.com/harriebird/malaybalay-covid/-/blob/master/LICENSE)

## Change list
* pipenv support (28/03/2020)
* production ready configs (28/03/2020)
